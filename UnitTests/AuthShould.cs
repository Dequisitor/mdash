﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MDash.Auth;
using MDash.Data.Models;
using MDash.Data;
using System.Data.Entity.Infrastructure;
using Mdash.Data.Interfaces;

namespace UnitTests {

	[TestClass]
	public class AuthShould {

		[TestMethod]
		public void RejectInvalidCredentials() {
			Auth auth = new Auth();
			Assert.AreEqual(false, auth.LoginUser("", ""));
			Assert.AreEqual(false, auth.LoginUser("asd@.com", ""));
			Assert.AreEqual(false, auth.LoginUser("@asd@asd.com", ""));
			Assert.AreEqual(false, auth.LoginUser("asd@asd.com", "sad"));
			Assert.AreEqual(false, auth.LoginUser("asd@asd.com", "sad"));
			Assert.AreEqual(false, auth.LoginUser("asd@asd.com", "sadASD1"));
			Assert.AreEqual(null, auth.SignupUser("", ""));
			Assert.AreEqual(null, auth.SignupUser("", "sadASD123"));
			Assert.AreEqual(null, auth.SignupUser("sdzxc@zxc", "sadASD123"));
			Assert.AreEqual(null, auth.SignupUser("sdzxc@zxc", "sadAS3"));
		}

		[TestMethod]
		public void RejectUnregistered() {
			Auth auth = new Auth();
			bool unreg = auth.LoginUser("test@testunreg.com", "asd123ASD");
			Assert.AreEqual(false, unreg);
		}

		[TestMethod]
		public void SignupValidUser() {
			Repository repo = new Repository(new UserContext());
			Validator validator = new Validator();
			ErrorHandler error = new ErrorHandler();
			PasswordHandler pass = new PasswordHandler();
			Auth auth = new Auth(validator, repo, error, pass);

			int? id = null;
			try {
				id = auth.SignupUser("test@test.com", "asdZXC123");
				Assert.IsNotNull(id);
			} catch (Exception ex) {
				Assert.Fail(ex.Message);
			} finally {
				if (id != null) {
					repo.RemoveUser(id ?? default(int));
					repo.Save();
				}
				repo.Dispose();
			}
		}

		[TestMethod]
		public void AllowSignUpAndLogin() {
			Repository repo = new Repository(new UserContext());
			Validator validator = new Validator();
			ErrorHandler error = new ErrorHandler();
			PasswordHandler pass = new PasswordHandler();
			Auth auth = new Auth(validator, repo, error, pass);

			string email = "auth@testlogin.com";
			int? id = null;
			try {
				id = auth.SignupUser(email, "asdQWE123");

				Assert.AreEqual(true, auth.LoginUser(email, "asdQWE123"));
			} catch (Exception ex) {
				Assert.Fail(ex.Message);
			} finally {
				cleanUp(repo, email);
				repo.Dispose();
			}
		}

		[TestMethod]
		public void RejectBadPassword() {
			Repository repo = new Repository(new UserContext());
			Validator validator = new Validator();
			ErrorHandler error = new ErrorHandler();
			PasswordHandler pass = new PasswordHandler();
			Auth auth = new Auth(validator, repo, error, pass);

			string email = "auth@testbad.com";
			int? id = null;
			try {
				id = auth.SignupUser(email, "asdQWE123");
				Assert.AreEqual(false, auth.LoginUser("auth@test.com", "asdQWE456"));
			} catch (Exception ex) {
				Assert.Fail(ex.Message);
			} finally {
				cleanUp(repo, email);
				repo.Dispose();
			}
		}

		[TestMethod]
		public void NotAllowDuplicateSignUps() {
			Repository repo = new Repository(new UserContext());
			Validator validator = new Validator();
			ErrorHandler error = new ErrorHandler();
			PasswordHandler pass = new PasswordHandler();
			Auth auth = new Auth(validator, repo, error, pass);

			string email = "auth@testdupe.com";
			int? id = null;
			try {
				id = auth.SignupUser(email, "asdQWE123");
				auth.SignupUser(email, "asdQWE456");

				Assert.Fail("Signup allowed duplicates");
			} catch (DbUpdateException ex) {
				//success
			} finally {
				cleanUp(repo, email);
				repo.Dispose();
			}
		}

		private void cleanUp(IRepository repo, string email) {
			User user = repo.GetUserByEmail(email);
			if (user != null) {
				repo.RemoveUser(user.UserId);
				repo.Save();
			}
		}

	}
}
