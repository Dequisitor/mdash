﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MDash.Data.Models;

namespace UnitTests {

	[TestClass]
	public class UserModelShould {

		[TestMethod]
		public void HashPassword() {
			string password = "asdQWE123";
			User user = new User { Email = "test@test.com", Password = password };
			Assert.AreNotEqual(password, user.HashedPassword);
		}
	}
}
