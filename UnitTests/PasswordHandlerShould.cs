﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MDash.Data;
using MDash.Data.Models;
using System.Security.Cryptography;

namespace UnitTests {
	[TestClass]
	public class PasswordHandlerShould {
		[TestMethod]
		public void VerifyValidPassword() {
			var passwordHandler = new PasswordHandler();
			string password = "abcDEF123";

			User user = new User { Email = "verify@test.com", Password = password };
			Assert.AreEqual(true, passwordHandler.VerifyPassword(password, user.HashedPassword, user.Salt));
		}

		[TestMethod]
		public void RejectBadPassword() {
			string password = "abcDEF123";
			User user = new User { Email = "verify@test.com", Password = password };
			var passwordHandler = new PasswordHandler();

			Assert.AreEqual(false, passwordHandler.VerifyPassword("abcDEF124", user.HashedPassword, user.Salt));
			Assert.AreEqual(false, passwordHandler.VerifyPassword("AbcDEF123", user.HashedPassword, user.Salt));
			Assert.AreEqual(false, passwordHandler.VerifyPassword("abcDeF123", user.HashedPassword, user.Salt));
		}
	}
}
