﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MDash.Auth;

namespace UnitTests {

	[TestClass]
	public class ValidatorShould {

		[TestMethod]
		public void RejectOnEmptyEmail() {
			Validator validator = new Validator();
			Assert.AreEqual(false, validator.isValidEmail(""));
		}

		[TestMethod]
		public void RejectOnEmptyPassword() {
			Validator validator = new Validator();
			Assert.AreEqual(false, validator.isValidPassword(""));
		}

		[TestMethod]
		public void RejectInvalidEmails() {
			Validator validator = new Validator();
			Assert.AreEqual(false, validator.isValidEmail("asda.asdad.asdasd"));
			Assert.AreEqual(false, validator.isValidEmail("as"));
			Assert.AreEqual(false, validator.isValidEmail("asda.asdad@asdasd"));
			Assert.AreEqual(false, validator.isValidEmail("asda.asdad@asdasd.q"));
			Assert.AreEqual(false, validator.isValidEmail("asda@asdad@asdasd.com"));
			Assert.AreEqual(false, validator.isValidEmail("asda asdad@asdasd.com"));
			Assert.AreEqual(false, validator.isValidEmail("asda.asdad@asdasd.comm"));
			Assert.AreEqual(false, validator.isValidEmail("asda.asdad@asdasd.asdasdasd"));
			Assert.AreEqual(false, validator.isValidEmail(".asda.asdad.asdasd"));
			Assert.AreEqual(false, validator.isValidEmail("@asda.asdad.asdasd"));
			Assert.AreEqual(false, validator.isValidEmail("asda.asdad.asdasd@"));
			Assert.AreEqual(false, validator.isValidEmail("asdá@google.com"));
			Assert.AreEqual(false, validator.isValidEmail("asd-sd@google.com"));
		}

		[TestMethod]
		public void AllowValidEmails() {
			Validator validator = new Validator();
			Assert.AreEqual(true, validator.isValidEmail("asdad@ads.de"));
			Assert.AreEqual(true, validator.isValidEmail("asdad@ads.com"));
			Assert.AreEqual(true, validator.isValidEmail("asda.ad@ads.de"));
			Assert.AreEqual(true, validator.isValidEmail("a.asdad@adssdasd.de"));
			Assert.AreEqual(true, validator.isValidEmail("asd.ad@ads.de"));
			Assert.AreEqual(true, validator.isValidEmail("asdad@ads.com"));
			Assert.AreEqual(true, validator.isValidEmail("asdad@adssdzxcz.com"));
			Assert.AreEqual(true, validator.isValidEmail("asda.asd.zxcd@ads.de"));
		}

		[TestMethod]
		public void RejectInvalidPasswords() {
			Validator validator = new Validator();
			Assert.AreEqual(false, validator.isValidPassword("apple"));
			Assert.AreEqual(false, validator.isValidPassword("Apple"));
			Assert.AreEqual(false, validator.isValidPassword("aaaapple8"));
			Assert.AreEqual(false, validator.isValidPassword("app8le"));
			Assert.AreEqual(false, validator.isValidPassword("apppppplE"));
			Assert.AreEqual(false, validator.isValidPassword("Apple9"));
			Assert.AreEqual(false, validator.isValidPassword("APPLKLJKJE"));
			Assert.AreEqual(false, validator.isValidPassword("999999992831"));
			Assert.AreEqual(false, validator.isValidPassword("99999999aasd"));
			Assert.AreEqual(false, validator.isValidPassword("ASDAPPLASDE9"));
		}

		[TestMethod]
		public void AllowValidPasswords() {
			Validator validator = new Validator();
			Assert.AreEqual(true, validator.isValidPassword("ApplePeach9"));
			Assert.AreEqual(true, validator.isValidPassword("9PeachApple9"));
			Assert.AreEqual(true, validator.isValidPassword("Apricot7Apple9"));
			Assert.AreEqual(true, validator.isValidPassword("PlumApple9"));
		}
	}
}
