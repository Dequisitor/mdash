﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MDash.Data;
using MDash.Data.Models;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity.Infrastructure;

namespace UnitTests {

	[TestClass]
	public class RepoShould {

		[TestMethod]
		public void FailWithInvalidContext() {
			try {
				Repository repo = new Repository(null);
				Assert.Fail("did not fail");
			} catch (Exception ex) {
				//failed, which is what we wanted
			}
		}

		[TestMethod]
		public void PersistUser() {
			User user = new User() { Email = "test@test.com", Password = "asdZXC123" };
			using (var repo = new Repository(new UserContext())) {
				repo.InsertUser(user);
				repo.Save();
			}

			using (var repo = new Repository(new UserContext())) {
				User newUser = repo.GetUserByEmail(user.Email);
				Assert.IsNotNull(newUser);

				repo.RemoveUser(newUser.UserId);
				repo.Save();
			}
		}

		[TestMethod]
		public void NotAllowSameEmail() {
			User user = new User { Email = "test@newemail.com", Password = "asdZXC1234" };
			Repository repo = new Repository(new UserContext());
			try {
				repo.InsertUser(user);
				repo.Save();
				repo.InsertUser(user);
				repo.Save();

				//if there was no exception then fail the test
				Assert.Fail("Db allowed duplicates");
			} catch (DbUpdateException ex) {
				//success
			} catch (Exception ex) {
				Assert.Fail(ex.Message);
			} finally {
				repo.Dispose();
				repo = new Repository(new UserContext());
				int id = repo.GetUserByEmail(user.Email).UserId;
				repo.RemoveUser(id);
				repo.Save();
				repo.Dispose();
			}
		}
	}
}
