# MDash #

Login/Signup class library with user persistence written in C#.

## Details
The solution consists of 3 projects:

- **Auth** - main project, handles login/signup
- **Data** - persistence layer, using entity framework with codefirst, applying repository pattern
- **UnitTests** - tests for the application

### Auth
Exposes two main functions: LoginUser and SignupUser. Both take an email and a password. Both validate their inputs: email check with regex, password must be at least 8 characters long, must have lowercase, uppercase letters and numbers. 
If validation passes Signup will create a user and save it in the DB. It's return value is the ID of the user (or null if fails).
If the credentials are valid, the Login process will try to retrieve the user with the same email from the DB, then compares hashed passwords, and returns a boolean with true value, if the password was correct. Password is actually never stored, only a salt and the hashed password.
#### Dependencies
- **Validator** - a class that validates emails and passwords. Check interface for details.
- **Repository** - for DB usage. (interface)
- **ErrorHandler** - helper class to create error log.
- **PasswordHandler** - a class that handles password hashing, creating salts and verifying passwords. (interface)
- There are two constructors, one that adheres to the SOLID principle of DI (4 parameters for the 4 dependencies), and a default one, that instantiates the class with default dependencies.

### Data
Persistence layer with CRUD operations. Uses localdb. It's only dependency is the DbContext, which can be set in the constructor. Codefirst migrations are also here.
User model processes the password on a SET operation, and creates a salt with a hashed password. The password is excluded from the db, only the salt and the hashed password are stored.

### UnitTests
During the creation of the project I used TDD. Therefore Unit tests were a vital part of the development. After a few (5-6) wasted hours trying to start with .Net core (dnxcore50 an dnx451) and **xunit** I decided net452 is the best and easiest way to create the infrastructure for TDD.
All modules of the application have tests (except IErrorHandler). Since the application is very small, I could not find myself in a situation where mock objects would be really necessary. Although thanks to IoC, all the classes are prepared for this.

## Additional notes
- Email is a unique field for the User model. If duplicates are inserted and exception will be thrown. This is intentionally not handled by the solution, I believe this is an error that should be handled by the client.
- User update and delete are implemented in the repository, they can be used.
- You will probably find very few comments, and hopefully thanks to the shortness of functions/classes they won't be missed.