﻿using MDash.Auth.Interfaces;
using System.Text.RegularExpressions;

namespace MDash.Auth {
	public class Validator : IValidator {

		public bool isValidEmail(string email) {
			Regex regex = new Regex(@"^[a-zA-Z0-9]+(\.[a-zA-Z0-9]+)*@[a-zA-Z0-9]{3,}\.[a-zA-Z0-9]{2,3}$");
			return regex.IsMatch(email);
		}

		public bool isValidPassword(string password) {
			Regex lowerCase = new Regex(@"[a-z]");
			Regex upperCase = new Regex(@"[A-Z]");
			Regex number = new Regex(@"[0-9]");

			return
				password.Length > 7 &&
				lowerCase.IsMatch(password) &&
				upperCase.IsMatch(password) &&
				number.IsMatch(password);
		}
	}
}
