﻿using MDash.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDash.Auth.Interfaces {
	public interface ILoginUser {
		bool LoginUser(string email, string password);
	}
}
