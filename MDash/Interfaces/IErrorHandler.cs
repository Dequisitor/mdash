﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDash.Auth.Interfaces {
	public interface IErrorHandler {
		string getLastError();
		void setError(string error);
	}
}
