﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDash.Data.Models;

namespace MDash.Auth.Interfaces {
	public interface ISignupUser {
		int? SignupUser(string email, string password);
	}
}
