﻿using MDash.Auth.Interfaces;
using MDash.Data.Models;
using Mdash.Data.Interfaces;
using MDash.Data;
using System.Security.Cryptography;
using MDash.Data.Interfaces;

namespace MDash.Auth {
	public class Auth : ILoginUser, ISignupUser {
		private IRepository _repository;
		private IValidator _validator;
		private IErrorHandler _errorHandler;
		private IPasswordHandler _passwordHandler;

		public Auth(IValidator validator, IRepository repository, IErrorHandler errorHandler, IPasswordHandler passwordHandler) {
			this._validator = validator;
			this._repository = repository;
			this._errorHandler = errorHandler;
			this._passwordHandler = passwordHandler;
		}

		public Auth() {
			this._validator = new Validator();
			this._repository = new Repository(new UserContext()); //can be refactored
			this._errorHandler = new ErrorHandler();
			this._passwordHandler = new PasswordHandler(new RNGCryptoServiceProvider(), new SHA256Managed()); //can be refactored
		}

		public bool LoginUser(string email, string password) {
			bool isValidUser = this._validator.isValidEmail(email) && this._validator.isValidPassword(password);
			if (!isValidUser) {
				this._errorHandler.setError("Invalid user credentials");
				return false;
			}

			//is user registered
			User fromDb = this._repository.GetUserByEmail(email);
			if (fromDb == null) {
				this._errorHandler.setError($"No registered user can be found with this email ({email})");
				return false;
			}

			//hash password, check if they match
			return this._passwordHandler.VerifyPassword(password, fromDb.HashedPassword, fromDb.Salt);
		}

		public int? SignupUser(string email, string password) {
			bool isValidUser = this._validator.isValidEmail(email) && this._validator.isValidPassword(password);
			if (!this._validator.isValidEmail(email)) {
				this._errorHandler.setError("Invalid email");
				return null;
			}
			if (!this._validator.isValidPassword(password)) {
				this._errorHandler.setError("Invalid password");
				return null;
			}

			//exceptions should be handled by the client (like duplicate user, etc)
			User user = new User() { Email = email, Password = password };
			_repository.InsertUser(user);
			_repository.Save();
			return user.UserId;
		}
	}
}
