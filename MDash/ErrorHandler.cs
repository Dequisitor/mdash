﻿using MDash.Auth.Interfaces;

namespace MDash.Auth {
	public class ErrorHandler : IErrorHandler {
		private string _error;
		public string getLastError() {
			return this._error;
		}

		public void setError(string error) {
			this._error = error;
		}
	}
}
