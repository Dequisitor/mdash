﻿using MDash.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mdash.Data.Interfaces {
	public interface IRepository : IDisposable {
		void InsertUser(User user);
		void RemoveUser(int id);
		void UpdateUser(User user);
		User GetUserById(int id);
		User GetUserByEmail(string email);
		IEnumerable<User> GetUsers();
		void Save();
	}
}
