﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDash.Data.Interfaces {
	public interface IPasswordHandler {
		string CreateSalt(int size);
		string HashPassword(string salt, string password);
		bool VerifyPassword(string password, string hash, string salt);
	}
}
