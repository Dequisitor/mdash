﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MDash.Data.Models {

	public class User{
		private PasswordHandler _passHandler;

		public User(PasswordHandler passHandler) {
			this._passHandler = passHandler;
		}

		//default constructor
		public User() {
			var rng = new RNGCryptoServiceProvider();
			var alg = new SHA256Managed();
			this._passHandler = new PasswordHandler(rng, alg);
		}

		[Key]
		public int UserId { get; set; }

		[MaxLength(100)]
		[Required]
		[Index(IsUnique = true)]
		public string Email { get; set; }

		private string _password;

		[NotMapped]
		public string Password {
			get { return _password; }
			set {
				_password = value;
				Salt = this._passHandler.CreateSalt();
				HashedPassword = this._passHandler.HashPassword(Salt, value);
			}
		}

		[Required]
		public string HashedPassword { get; set; }

		[Required]
		public string Salt { get; set; }
	}
}
