﻿using MDash.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace MDash.Data.Models {

	public class UserContext: DbContext {

		public UserContext() : base("name=UserContext") {
		}

		public DbSet<User> Users { get; set; }
	}
}
