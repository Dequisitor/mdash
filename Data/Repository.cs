﻿using MDash.Data.Models;
using Mdash.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDash.Data
{
	public class Repository: IRepository, IDisposable
	{
		private UserContext _userContext;

		public Repository(UserContext context) {
			this._userContext = context;
		}

		public User GetUserById(int id) {
			return this._userContext.Users.Find(id);
		}

		public User GetUserByEmail(string email) {
			return this._userContext.Users.Where(t => t.Email == email).FirstOrDefault();
		}

		public IEnumerable<User> GetUsers() {
			return this._userContext.Users.ToList();
		}

		public void InsertUser(User user) {
			this._userContext.Users.Add(user);
		}

		public void RemoveUser(int id) {
			var user = this._userContext.Users.Find(id);
			if (user != null) {
				this._userContext.Users.Remove(user);
			} 
		}

		public void Save() {
			this._userContext.SaveChanges();
		}

		public void UpdateUser(User user) {
			this._userContext.Entry(user).State = System.Data.Entity.EntityState.Modified;
		}

		private bool disposed = false;
		protected virtual void Dispose(bool disposing) {
			if (!this.disposed) {
				if (disposing) {
					this._userContext.Dispose();
				}
			}
			this.disposed = true;
		}
		public void Dispose() {
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}
