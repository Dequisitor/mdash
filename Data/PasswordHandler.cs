﻿using MDash.Data.Interfaces;
using MDash.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MDash.Data {
	public class PasswordHandler: IPasswordHandler {
		private HashAlgorithm _alg;
		private RandomNumberGenerator _rng;

		//for testing purposes, with default dependencies
		public PasswordHandler() {
			this._rng = new RNGCryptoServiceProvider();
			this._alg = new SHA256Managed();
		}

		public PasswordHandler(RandomNumberGenerator rng, HashAlgorithm alg) {
			this._rng = rng;
			this._alg = alg;
		}

		public string CreateSalt(int size = 32) {
			var salt = new byte[size];
			this._rng.GetBytes(salt);
			return Encoding.ASCII.GetString(salt);
		}

		public string HashPassword(string salt, string password) {
			var saltAndPass = String.Concat(salt, password);

			byte[] plainSaltAndPass = Encoding.ASCII.GetBytes(saltAndPass);
			byte[] hashedBytes = this._alg.ComputeHash(plainSaltAndPass);

			return Encoding.ASCII.GetString(hashedBytes);
		}

		public bool VerifyPassword(string password, string hash, string salt) {
			var hashedPass = HashPassword(salt, password);
			return hashedPass.Equals(hash);
		}

	}
}
