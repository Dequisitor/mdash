// <auto-generated />
namespace MDash.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class UserContraints : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(UserContraints));
        
        string IMigrationMetadata.Id
        {
            get { return "201603271049313_UserContraints"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
