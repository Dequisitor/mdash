namespace MDash.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserContraints : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "Email", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Email", c => c.String());
        }
    }
}
