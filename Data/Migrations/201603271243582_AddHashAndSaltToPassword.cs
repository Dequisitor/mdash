namespace MDash.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddHashAndSaltToPassword : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Users", new[] { "Email" });
            CreateTable(
                "dbo.UserModels",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        Email = c.String(nullable: false, maxLength: 100),
                        HashedPassword = c.String(nullable: false),
                        Salt = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.UserId)
                .Index(t => t.Email, unique: true);
            
            DropTable("dbo.Users");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        Email = c.String(maxLength: 100),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            DropIndex("dbo.UserModels", new[] { "Email" });
            DropTable("dbo.UserModels");
            CreateIndex("dbo.Users", "Email", unique: true);
        }
    }
}
